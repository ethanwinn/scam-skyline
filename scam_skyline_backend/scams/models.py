from django.contrib.gis.db import models

class Scam(models.Model):
    geometry = models.GeometryField(blank=True, null=True)
    name = models.CharField(max_length=512)

    def __unicode__(self):
        return self.name
